﻿using System;
using System.Windows;

namespace HiVE.GUIDGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    internal partial class MainWindow : Window
    {
        public MainWindow()
        {
            oCurrentGUID = new Guid();

            InitializeComponent();
        }

        private static System.Guid oCurrentGUID { get; set; }

        private void buttonNewGUIDPlus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                textBlockState.Text = "Start Creade NewGUID!";

                oCurrentGUID = System.Guid.NewGuid();
                Clipboard.SetText(oCurrentGUID.ToString());
                textBoxCurrentGUID.Text = oCurrentGUID.ToString();

                textBlockState.Text =
                    string.Format(
                        "Creaded NewGUID in DateTime: {0} !",
                        DateTime.Now.ToString("yyyy/MM/dd : HH.mm.ss"));
            }
            catch
            {
                textBlockState.Text = "IsError in Creade NewGUID!";
            }
        }

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                textBlockCopyright.Text = Models.AssemblyAttributeAccessors.EntryAssemblyCopyright;
            }
            catch { }
        }
    }
}
